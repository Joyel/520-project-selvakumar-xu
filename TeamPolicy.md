# Team Policy

## General Information

Coordinators: Joyel Selvakumar, Jimmy Xu
Monitor: Jimmy Xu
Checker: Joyel Selvakumar

## Common Meeting Times

* Wednesday during allotted lab time

* At least one Discord session, possibly one additional in-person session

(Reasonably, it is unlikely to have the same free time periods every week.
As needed, we will discuss and plan the meeting times in advance of 24 hours.)

## In Case of Meeting Disruptions

Notify in messaging about inability to attend any given meetings. Suggest
alternative time. Respond to missed messages the soonest possible.

## Delay of Communication

Within 24 hours of message

## Code Review Delay

Due to difficulty in a consistent schedule, not fixed. Ideally within a few hours,
but realistically within the day.

## Team Standards And Expectations

As a team, we agree to:

* Show commitment and dedication to working on the project adequately and equally
* Agree on the design of the end product
* Show respect to one another
* Divide the work reasonably, and discuss that division of work
* Outline and appreciate task priorities for the project's design
* Maintain punctuality on completing our assigned work to the best of our abilities
* Participate sufficiently in the work flow and progress week by week
* Help each other when needed
* Communicate and discuss issues related to the project's progress and content
* Communicate potential issues or disruptions to agreed meeting times promptly
* Provide sufficient progress and status updates on our work
* Update and fill out reports, logs, etc. punctually
* Provide and accept constructive criticism
* Discuss suggestions considerately, reiterating the task prioritization and timing