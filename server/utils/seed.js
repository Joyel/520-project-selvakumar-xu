const DB = require('../db/db.js');
const read = require('../controllers/readData.js');



/**
 * Load data from data file and inserts them into db collection 'inspections' only if its empty
 *
 * @returns {int} the number of records added
*/
async function seed(){
  let db;
  try {
    const data = await read('./server/data/data.geojson');
    const db = new DB();
    await db.connect('cluster0', 'inspections');
    const recordsFromDb = await db.readAllData();
    if(recordsFromDb.length === 0){
      return await db.addAllData(data);
    }
    return 0;
  } catch (e) {
    console.error('could not seed');
  } finally {
    if (db) {
      db.close();
    }
  }
}

module.exports = seed;