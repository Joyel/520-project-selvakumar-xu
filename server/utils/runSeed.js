const seed = require('./seed.js');

(async () => {
  await seed();
  process.exit();
})();