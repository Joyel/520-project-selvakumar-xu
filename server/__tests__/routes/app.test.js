const request = require('supertest');
const app = require('../../app');
const DB = require('../../db/db');

jest.mock('../../db/db');

describe('GET /establishments/:name ', () => {
  test('Responds with a JSON array', async () => {
    const resolvedValue = [{'city':'test', 'zipcode':'11111', 'statecode':'TS', 'trade_name':'test',
      'inspection_grade':'A', 'owner_name':'test', 'county':'test', 'establishment_type':'T',
      'street':'test', 'coordinates':[-73.963993422, 42.724850337], 
      'inspections':[{'date':'2022-04-18T00:00:00.000', 'deficiencies':[null]}]}];
    jest.spyOn(DB.prototype, 'readEstablishmentDetails').mockResolvedValue(
      resolvedValue);
    const response = await request(app).get('/api/v1/establishments/ALADDIN HALAL');
    expect(response.body).toEqual(resolvedValue[0]);
    expect(response.statusCode).toBe(200);
    expect(response.type).toEqual('application/json');
  });

  test('Responds with a 404 Establishment not found', async () => {
    jest.spyOn(DB.prototype, 'readEstablishmentDetails').mockResolvedValue([]);
    const response = await request(app).get('/api/v1/establishments/fake');
    expect(response.body).toEqual({'status': 404, 'error': 
    'Specified establishment not found'});
    expect(response.statusCode).toBe(404);
  });
});

describe('GET /grades/:grade/establishments ', () => {
  test('Responds with a JSON array', async () => {
    const resolvedValue = [{'trade_name':'test', 'inspection_grade':'A', 
      'coordinates':[-73.963993422, 42.724850337]}];
    jest.spyOn(DB.prototype, 'readAllOfGrade').mockResolvedValue(resolvedValue);
    const response = await request(app).get('/api/v1/grades/A/establishments');
    expect(response.body).toEqual(resolvedValue);
    expect(response.statusCode).toBe(200);
    expect(response.type).toEqual('application/json');
  });

  test('Responds with 404 Grade must be between A and C', async () => {
    const response = await request(app).get('/api/v1/grades/Z/establishments');
    expect(response.body).toEqual({'status': 404, 'error': 'Grade must be between A and C'});
    expect(response.statusCode).toBe(404);
  });
});