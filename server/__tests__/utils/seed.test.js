const seed = require('../../utils/seed.js');
const DB = require('../../db/db.js');
const fs = require('node:fs/promises');

jest.mock('../../db/db');
jest.mock('../../controllers/readData');

test('Returns a string', async() => {
  fs.readFile = jest.fn().mockResolvedValue('"inspection 1"');
  //we don't want to make a connection everytime we run a test
  jest.spyOn(DB.prototype, 'connect').mockResolvedValue();
  //we won't have to close connection if we didnt make one
  jest.spyOn(DB.prototype, 'close').mockResolvedValue();
  jest.spyOn(DB.prototype, 'addAllData').mockResolvedValue(1);
  jest.spyOn(DB.prototype, 'readAllData').mockResolvedValue([]);
  expect(await seed()).toEqual(1);
});

test('Returns a string', async() =>{
  fs.readFile = jest.fn().mockResolvedValue('"inspection 1"');
  //we don't want to make a connection everytime we run a test
  jest.spyOn(DB.prototype, 'connect').mockResolvedValue();
  //we won't have to close connection if we didnt make one
  jest.spyOn(DB.prototype, 'close').mockResolvedValue();
  jest.spyOn(DB.prototype, 'addAllData').mockResolvedValue(1);
  jest.spyOn(DB.prototype, 'readAllData').mockResolvedValue(['inspection 1']);
  expect(await seed()).toEqual(0);
});