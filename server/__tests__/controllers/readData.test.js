const readData = require('../../controllers/readData.js');
const fs = require('node:fs/promises');

test('Returns a string', async() =>{
  fs.readFile = jest.fn().mockResolvedValue('"data is read"');
  const theData = await readData('shouldnt matter');
  expect(theData).toEqual('data is read');
});