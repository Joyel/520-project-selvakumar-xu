const fs = require('node:fs/promises');

/**
 * Read a file and parse into json
 *
 * @param {string} file path to file to read from
 * @returns {Object} json of file read from
 * @throws {Exception} if file can't be found or if data can't be parsed
*/
async function read(file) {
  try {
    const data = await fs.readFile(file, 'utf-8');
    return JSON.parse(data);
  } catch (err) {
    console.error(err.message);
    process.exit();
  }
}

module.exports = read;