const express = require('express');
const DB = require('../db/db');

const router = express.Router();
const db = new DB();

/**
 * Queries MongoDB collection for single document corresponding to establishment name query param
 *
 * @param {Object} req Object representing client request data
 * @param {Object} res Object representing server response data
 * @returns {Object} Single MongoDB collection object with an inspection report's data
*/
router.get('/:name', async (req, res) => {
  if (req.params.name){
    if (!req.params.name.match(/^[a-zA-Z0-9 ]+$/)){
      res.status(404);
      res.json({'status': 404, 'error': 'Only letters, numbers and spaces are allowed'});
    }
    try{
      let processedQuery = req.params.name.toUpperCase();
      processedQuery = processedQuery.replace(/-/g, ' ');
      const establishmentDetails = await db.readEstablishmentDetails(processedQuery);
      if (establishmentDetails.length > 0){
        res.set({'Cache-Control': 'no-cache'});
        res.json(establishmentDetails[0]);
      }else{
        res.status(404);
        res.json({'status': 404, 'error': 'Specified establishment not found'});
      }
    }catch (e){
      console.error(e.message);
      res.status(500);
      res.json({'status': 500, 'error': 'An Error occurred. Please try again later.'});
    }
  }else{
    res.status(404);
    res.json({'status': 404, 'error': 'Specified page not found'});
  }
});

module.exports = router;