const express = require('express');
const DB = require('../db/db');

const router = express.Router();
const db = new DB();

/**
 * Queries MongoDB collection for documents corresponding to grade query param
 *
 * @param {Object} req Object representing client request data
 * @param {Object} res Object representing server response data
 * @returns {Object} Array of MongoDB collection objects with inspection report data
 */
router.get('/:grade/establishments', async (req, res) => {
  if (req.params.grade){
    if (req.params.grade.match(/^[a-cA-C]$/)){
      try{
        const establishmentDetails = await db.readAllOfGrade(req.params.grade.toUpperCase());
        res.set({'Cache-Control': 'no-cache'});
        res.json(establishmentDetails);
      }catch (e){
        console.error(e.message);
        res.status(500);
        res.json({'status': 500, 'error': 'An Error occurred. Please try again later.'});
      } 
    }else{
      res.status(404);
      res.json({'status': 404, 'error': 'Grade must be between A and C'});
    }
  }else{
    res.status(404);
    res.json({'status': 404, 'error': 'Specified page not found'});
  }
});

module.exports = router;