const express = require('express');
const grade = require('./routes/grade.js');
const establishment = require('./routes/establishment.js');

const app = express();

const compression = require('compression');
app.use(compression());

app.use(express.static('./client/build'));
app.use('/api/v1/grades', grade);
app.use('/api/v1/establishments', establishment);


/**
 * Create error response for invalid requests from client
 *
 * @param {Object} req Object representing client request data
 * @param {Object} res Object representing server response data
 * @returns {Object} Response object with error message (and specified error code in header)
*/
app.use((req, res) => {
  const errObj = {'status': 404, 'error': 'Page not found'};
  res.status(404);
  res.json(errObj);
});

module.exports = app;