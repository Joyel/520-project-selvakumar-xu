require('dotenv').config();
const dbUrl = process.env.ATLAS_URI;
const { MongoClient } = require('mongodb');

let instance = null; 

class DB {

  /**
   * Creates a DB singleton, if needed (if null), returns it
   *
   * @returns {Object} Instance of the Database object (singleton)
  */
  constructor(){
    if (!instance){
      instance = this;
      this.client = new MongoClient(dbUrl);
      this.db = null;
      this.collection = null;
    }
    return instance;
  }

  /**
   * Connects to the specified database and collection
   *
   * @param {String} dbName name of the database to connect
   * @param {String} collName of the collection to connect
   * @returns {Object} Instance of the Database object (singleton)
  */
  async connect(dbName, collName) {
    if (instance.db){
      return;
    }
    await instance.client.connect();
    instance.db = await instance.client.db(dbName);
    await instance.client.db(dbName).command({ ping: 1 });
    instance.collection = await instance.db.collection(collName);
  }

  /**
   * Closes the connection to the MongoDB
   *
   * @returns null
  */
  async close() {
    await instance.client.close();
    instance = null;
  }

  /**
   * Connects to the specified database and collection, and also performs cleanup (closes)
   *
   * @param {String} dbName name of the database to connect
   * @param {String} collName of the collection to connect
   * @returns null
  */
  async open(dbname, collName) {
    try {
      await instance.connect(dbname, collName);
    } finally {
      await instance.close();
    }
  }

  /**
   * Queries the MongoDB to retrieve results matching the specified query (letterGrade)
   *
   * @param {String} letterGrade, the value to filter the DB query results
   * @returns {Object} Array of MongoDB collection objects corresponding to the query
  */
  async readAllOfGrade(letterGrade) {
    const query = {'inspection_grade': letterGrade};
    const projection = {'trade_name': 1, 'inspection_grade': 1, 'coordinates': 1, 
      'establishment_type': 1};
    return await instance.collection.find(query).project(projection).toArray();
  }

  /**
   * Queries the MongoDB to retrieve results matching the specified query (name)
   *
   * @param {String} name, the value to filter the DB query results
   * @returns {Object} Array of single MongoDB collection object corresponding to the query
  */
  async readEstablishmentDetails(name) {
    const query = {'trade_name': name};
    return await instance.collection.find(query).toArray();
  }

  /**
   * Queries the MongoDB to retrieve all records
   *
   * @returns {Object} Array of all MongoDB collection objects in that database
  */
  async readAllData() {
    return await instance.collection.find().toArray();
  }

  /**
   * Inserts records (documents) into the MongoDB collection
   *
   * @param {Object} inspections, array of MongoDB collection objects (documents) to be added
   * @returns {Number} Total number of objects (documents) added into the database
  */
  async addAllData(inspections) {
    const result = await instance.collection.insertMany(inspections);
    return result.insertedCount;
  }
}

module.exports = DB;
