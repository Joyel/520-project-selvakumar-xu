Proposal.md

# Food Inspections Map

## Data

https://gitlab.com/csy3dawson23-24/520/teams/TeamA12-JimmyJoyel/520-project-selvakumar-xu/-/issues/1

## API

APIs:

* Establishment details (GET, /api/v1/establishments/:name)
Detailed information about a specific establishment is sent.


* Establishment Type Filter (GET, /api/v1/grades/:grade/establishments)
The results are filtered to one or more selected grade(s).

## Visualizations

The map provides a general idea of the layout of the region with respect to available food services
(ie: density and types of food service establishments, including with respect to districts, etc.).
More pertinently, it provides the users informative summaries about the safety and sanitary
conditions that may be expected from these services. This is, understandably, important information
in regards to making decisions on where to order food and shop groceries, impressions of the food
service sector, etc.
(No conclusions are made or described based on the reported data. Interpretation is the user's
responsibility.)

## Views

![layout wireframe](wireframe.png "Proposed Layout Wireframe")

(General View)
* Default display of the map, zoomed and centered on a selected part of New York state. The map
  occupies most of the view, from near the top.
* Filters to narrow the results down based on evaluation grade and food service establishment type.
* There will be markers on the map indicating establishments, narrowed down based on the filters.
  Clicking on a marker will show more details about that establishment below the fold.
* The story/summary for the overall data visualization will be placed below the page viewport,
  also visible by scrolling down.

Above the fold:
* Heading
* Map
* Control panel (filters)

Below the fold:
* Food inspection details panel
* Story/summary of visualization

## Functionality

Users can zoom in and out of the map, as well as move around. Users can click on a marker, which 
will display a popup showing the establishment's name and evaluation grade; it will also have an 
option to create a section below the fold with detailed information about the selected location, 
including the the inspection report and address information.

There will be 2 sets of buttons (selections can be stacked) that filter the establishment markers.
They are based on the establishment type and evaluation grade.

## Features and Priorities

The core features, which are the highest priority, are the elements described above.

Additional features, if time permits:
* Additional visual elements, such as map lines (ex: may connect establishments related by a 
  common factor)
* Color-coded markers, according to inspection grade
* User location as starting point of the map

## Dependencies

* React Leaflet library, an extension of Leaflet that is integrated with React JS

We chose this library as we really like its geographical map display and interactivity. It's 
informative since, as a baseline, the map provides street and general road visualizations, including 
their connections
to each other and where they go. So, it is even more interesting as a display when we can influence 
its displayed features and functionality based on what we code.

Also, for the data set we chose, the coordinates work really well with a geograhical map as a 
visualization.