# New York Food And Safety Inspections Map
## (Joyel Selvakumar, Jimmy Xu)

Full-Stack Web App (MERN)

## Description

This project is an interactive map of New York state that displays locations of
its food businesses that have undergone food and safety inspections. The
inspections were most recently updated on October 20, 2023. Users may click on
a restaurant location marker to learn more about both the restaurant and the
evaluation it received. Additionally, users may filter or customize the
displayed locations, such as by letter grade of evaluation.

This is a full-stack web app, implementing MongoDB, Express, React and NodeJS.

Planned Features (inclusion based on time):

* Use the user location as starting/center point of the map.

## Attributions

New York State Government for the provision of the [food and safety inspection
data](https://data.ny.gov/Economic-Development/Food-Safety-Inspections-Current-Ratings/d6dy-3h7r/) 
on its food businesses.
([New York State Department of Agriculture And Markets](https://agriculture.ny.gov/food-safety))

[React-Leaflet](https://react-leaflet.js.org/) is used client-side for the
display of the map of the STM metro network, as well as general UI interactions.

Map display from [OpenStreetMap](https://www.openstreetmap.org).


## Structure

The principal directories of the project are:

* `server/`: Back-end (Express server, using Node for MongoDB querying and other functions)
* `client/`: Front-end (React app + modules)

The root and each aforementioned directory contains a package.json file.
Each contains the respective dependencies, scripts to run, etc.

The server handles:

* API requests (routes)
* Serving the built React app, by the content of the client/build directory


## Setup

From the root directory, run

```
npm run build
```

to install the required dependencies.


### Seed The Database

Ensure you have the .env file in the project root, containing:
* PORT=3001
* ATLAS_URI=(your atlas URI, with username and password)

From the root directory, run

```
node server/utils/runSeed.js
```

### Client Only

If `client/package.json` has a `proxy` line, remove it. 

```
cd client
npm start
```

### Server Only

```
cd server
node app.js
```

### Client And Server

  - Note the port that the server is running on.
  - In `client\package.json`, add the following line as a `proxy`
    property:
    "proxy": "http://localhost:(#)", replacing `(#)` with the server port.

```
cd server
nodemon app.js
cd ../client
npm start
```
