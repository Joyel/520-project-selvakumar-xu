import './App.css';
import Main from './components/Main';

/**
 * @returns Datalist component, simple stub to display route data
 * 
 */
function App() {
  return (
    <>
      <header>
        <h1>New York Food And Safety Inspections</h1>
      </header>
      <main>
        <Main />
        <footer>
          <h3>Joyel Selvakumar, Jimmy Xu</h3>
        </footer>
      </main>
    </>
  );
}

export default App;
