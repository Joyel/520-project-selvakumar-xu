import { React, useState, useEffect } from 'react';
import NewYorkMap from './NewYorkMap';
import Controls from './Controls';
import Cards from './Cards';

/**
 * @returns Principal components of the React app
 * Top level component for our react app
 */
export default function Main() {
  const [markers, setMarkers] = useState(null);
  const [report, setReport] = useState(null);
  const [noData, setNoData]  = useState('');
  const [grades, setGrades] = useState([]);
  const [types, setTypes] = useState([]);
  
  useEffect(()=>{
    let newGrades = JSON.parse(localStorage.getItem('grades'));
    if (newGrades === null){
      newGrades = ['A'];
      setGrades(newGrades);
      localStorage.setItem('grades', JSON.stringify(newGrades));
    }else{
      setGrades(newGrades);
    }

    const localTypes = JSON.parse(localStorage.getItem('types'));
    if (localTypes === null){
      //const newTypes = ['A', 'B', 'C', 'E', 'K', 'V'];
      const newTypes = ['K', 'V'];
      setTypes(newTypes);
      localStorage.setItem('types', JSON.stringify(newTypes));
    }else{
      setTypes(localTypes);
    }

    for (const grade of newGrades){
      fetch(`/api/v1/grades/${grade}/establishments`).then((response) =>{
        if (!response.ok){
          throw new Error();
        }
        return response.json();
      }).then((data) =>{
        const newMarkers = [];
        for (const item of data){
          newMarkers.push(item);
        }
  
        setMarkers(newMarkers);
      }).catch((error)=>{
        setNoData(<p>Unable to load data. Try again later</p>);
      });
    }
  }, []);

  useEffect(()=>{
    const fetchUrl = '/api/v1/establishments/323 MAPLE LEAF';
    fetch(`${fetchUrl}`).then((response) => {
      if (!response.ok){
        throw new Error();
      }
      return response.json();
    }).then((data) => {
      setReport(data);
    }).catch((error)=>{
      setNoData(<p>Unable to load data. Try again later</p>);
    });
  }, []);

  /**
   * Fetch array of inspection grade objects to update marker state
   *
   * @param {string} letterGrade used as query param for fetch url
   * @returns {Object} array of summary data objects to be used as map markers
   * @throws {Exception} if fetch does not return a 200 ok response
  */
  function getGradedInspections(letterGrade){
    fetch(`/api/v1/grades/${letterGrade}/establishments`).then((response) =>{
      if (!response.ok){
        throw new Error();
      }
      return response.json();
    }).then((data) =>{
      const newMarkers = [...markers];
      for (const item of data){
        newMarkers.push(item);
      }

      setMarkers(newMarkers);
    }).catch((error)=>{
      if (markers.length === 0){
        setNoData(<p>Unable to load data. Try again later</p>);
      }
    });
  }

  /**
   * Filter array of markers (summary objects) based on types state variable
   *
   * @returns {Object} smaller array of summary data objects to be used as map markers
  */
  function filterMarkers(){
    const newMarkers = [];
    for (const type of types){
      for (const marker of markers){
        if (marker.establishment_type.includes(type) && notPresent(newMarkers, marker)){
          newMarkers.push(marker);
        }
      }
    }
    return newMarkers;
  }

  /**
   * Prevent addition of duplicates into the markers array
   *
   * @param {Object} newMarkers array of marker objects being checked for presence of marker to add
   * @param {Object} marker to potentially add to newMarkers, if it's not already present
   * @returns {boolean} whether marker is already present in newMarkers
  */
  function notPresent(newMarkers, marker){
    for (const eachMarker of newMarkers){
      if (eachMarker.trade_name === marker.trade_name){
        return false;
      }
    }
    return true;
  }

  /**
   * Add or remove grade from grades state variable
   *
   * @param {string} letterGrade to be added to the grades state variable
   * 
   * @returns void
  */
  function handleGrade(letterGrade){
    let newGrades = [...grades];
    if (!newGrades.includes(letterGrade)){
      newGrades.push(letterGrade);
      getGradedInspections(letterGrade);
    }else if (newGrades.length > 1){
      newGrades = newGrades.filter(grade => grade !== letterGrade);
      let newMarkers = [...markers];
      newMarkers = newMarkers.filter(marker => marker.inspection_grade !== letterGrade);
      setMarkers(newMarkers);
    }
    setGrades(newGrades);
    localStorage.setItem('grades', JSON.stringify(newGrades));
  }

  /**
   * Adding or remove type from types state variable
   *
   * @param {string} filterType to be added to the types state variable
   * 
   * @returns void
  */
  function handleType(filterType){
    let newTypes = [...types];
    if (!types.includes(filterType)){
      newTypes.push(filterType);
    }else if (newTypes.length > 1){
      newTypes = newTypes.filter(type => type !== filterType);
    }
    setTypes(newTypes);
    localStorage.setItem('types', JSON.stringify(newTypes));
  }

  /**
   * Fetches details about given inspection identified by tradeName
   * @param {String} tradeName id of database record to fetch
   * @returns {Object} inspection object with detailed properties
   */
  function updateReport(tradeName){
    const fetchUrl = `/api/v1/establishments/${tradeName}`;
    fetch(`${fetchUrl}`).then((response) => {
      if (!response.ok){
        throw new Error();
      }
      return response.json();
    }).then((data) => {
      setReport(data);
    }).catch((error)=>{
      setNoData(<p>Unable to load data. Try again later</p>);
    });
  }
  if (markers !== null && report !== null){
    const filteredMarkers = filterMarkers();
    return(
      <>
        <section className="map-container">
          <NewYorkMap markers={filteredMarkers} noData={noData} updateReport={updateReport} />
        </section>
        <Controls handleGrade={handleGrade} handleType={handleType} grades={grades} types={types}/>
        <br/>
        <Cards report={report}/>
      </>
    );
  }
  return <p>Unable to retrieve data. Please wait or try again later.</p>;
}