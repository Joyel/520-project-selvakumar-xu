import './Controls.css';

/**
 * @param {Object} handleGrade event handler for grade filter passed by Main
 * @param {Object} handleType event handler for type filter passed by Main
 * @param {Array} grades array of string containing the grade filter options selected by uer
 * @param {Array} types array of string contianer the type filter options selected by user
 * @returns Component for section with filter options, and also description & attribution
 */
export default function Controls({handleGrade, handleType, grades, types}){
  return <>
    <section id="attribution">
      <p>Consumers should be aware of the sanitation and risk associated with food products and the 
        establishments selling them. Services may be available that research and share this data 
        for the benefit of the public. One such service is provided by the New York state 
        government. Their freely available data is sampled to visualize geographically on this page.
      <a href="https://data.ny.gov/Economic-Development/Food-Safety-Inspecti
        ons-Current-Ratings/d6dy-3h7r/">
          Food and safety inspection data</a> provided by the New York state government (
      <a href="https://agriculture.ny.gov/food-safety">New York State Department of Ag
            riculture and Markets</a>).
      </p>
    </section>
    <p>Filter by inspection grade and establishment type</p>
    <section id="controls">
      <GradeControls grades={grades} handleGrade={handleGrade}/>
      <TypeControls types={types} handleType={handleType}/>
    </section>
  </>;
}

/**
  * @param {Array} types array of string contianer the type filter options selected by user
  * @param {Object} handleType event handler for type filter passed by Main
  * @returns Component for list of establishment type checkbox filters
 */
function TypeControls({types, handleType}){
  const labelList = getLabels();
  return <>
    <section id="type-filter">
      <div id="type-buttons">
        {
          labelList.map(function(label){
            let isChecked = false;
            for (const type of types){
              if (type === label[3]){
                isChecked = true;
                break;
              }
            }
            return(
              <label htmlFor={label[0]}>{label[1]}
                <input type="checkbox" id={label[0]} name={label[2]} value={label[3]}
                  checked={isChecked} onClick={() => handleType(label[3])}/>
              </label>
            );
          })
        }
      </div>
    </section>
  </>;
}

/**
 * @param {Array} grades array of string containing the grade filter options selected by uer
 * @param {Object} handleGrade event handler for grade filter passed by Main
 * @returns Component for grade button filters
 */
function GradeControls({grades, handleGrade}){
  const shadedSet = ['', '', ''];
  if (grades.includes('A')){
    shadedSet[0] = 'shaded';
  }
  const classA = `grade-button ${shadedSet[0]}`;
  if (grades.includes('B')){
    shadedSet[1] = 'shaded';
  }
  const classB = `grade-button ${shadedSet[1]}`;
  if (grades.includes('C')){
    shadedSet[2] = 'shaded';
  }
  const classC = `grade-button ${shadedSet[2]}`;
  return <>
    <section id="grade-filter">
      <div id="grade-buttons">
        <button className={classA} onClick={() => handleGrade('A')}>A</button>
        <button className={classB} onClick={() => handleGrade('B')}>B</button>
        <button className={classC} onClick={() => handleGrade('C')}>C</button>
      </div>
    </section>
  </>;
}

/**
   * Provide string values for each label-input pair component
   * @returns {Object} array of arrays, holding information to fill the label-input template
  */
export function getLabels(){
  return[
    ['store-type', 'Store', 'store', 'A'],
    ['bakery-type', 'Bakery', 'bakery', 'B'],
    ['food-manufacturer-type', 'Food Manufacturer', 'food-manufacturer', 'C'],
    ['food-warehouse-type', 'Food Warehouse', 'food-warehouse', 'D'],
    ['beverage-plant-type', 'Beverage Plant', 'beverage-plant', 'E'],
    ['non-medicated-feed-mill-type', 'Feed Mill (Non Medicated)', 'non-medicated-feed-mill', 'F'],
    ['medicated-feed-mill-type', 'Feed Mill (Medicated)', 'medicated-feed-mill', 'Q'],
    ['processing-plant-type', 'Processing Plant', 'processing-plant', 'G'],
    ['wholesale-manufacturer-type', 'Wholesale Manufacturer', 'wholesale-manufacturer', 'H'],
    ['refrigerated-warehouse-type', 'Refigerated Warehouse', 'refrigerated-warehouse', 'I'],
    ['vehicle-type', 'Vehicle', 'vehicle', 'K'],
    ['produce-refrigerated-warehouse-type', 'Produce Refrigerated Warehouse', 
      'produce-refrigerated-warehouse', 'L'],
    ['wholesale-producer-packer-type', 'Wholesale Producer Packer', 'wholesale-producer-packer', 
      'N'],
    ['produce-grower-packer-broker-storage-type', 'Produce Grower/Packer/Broker, Storage', 
      'produce-grower-packer-broker-storage', 'O'],
    ['controlled-atmosphere-room-type', 'Controlled Atmosphere Room', 'controlled-atmosphere-room', 
      'P'],
    ['pet-food-manufacturer-type', 'Pet Food Manufacturer', 'pet-food-manufacturer', 'R'],
    ['seed-warehouse-distributor-type', 'Seed Warehouse and/or Distributor', 
      'seed-warehouse-distributor', 'S'],
    ['disposable-plant-type', 'Disposable Plant', 'disposable-plant', 'T'],
    ['slaughterhouse-type', 'Slaughterhouse', 'slaughterhouse', 'V'],
    ['farm-winery-type', 'Farm Winery', 'farm-winery', 'W'],
  ];
}