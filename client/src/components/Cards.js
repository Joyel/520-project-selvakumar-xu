import './Cards.css';
import {getLabels} from './Controls';

/**
 * @returns Component representing list or series of detailed inspection reports
 * 
 */
export default function Cards({report}){
  
  return <>
    <Card report={report}/>
    <br/>
  </>;
}

/**
 * @returns Component representing headers and categorizing info of each inspection report
 * 
 */
function Card({report}){
  const types = [];
  for(const type of report.establishment_type){
    for(const label of getLabels()){
      if(label[3] === type){
        types.push(<li>{label[1]}</li>);
      }
    }
  }
  
  const inspections = [];
  for(const inspection of report.inspections){
    inspections.push(<Inspection inspection={inspection}/>);
  }
  return <>
    <section className="card">
      <section className="card-header">
        <h3>{report.trade_name}</h3>
        <p className={report.inspection_grade}>{report.inspection_grade.toUpperCase()}</p>
      </section>
      <p className="address">{report.city}, {report.street}, {report.county}, 
        {report.state}, {report.zipcode}</p>
      <ul className="establishment-type-list">
        {types}
      </ul>
      <br/>
      {inspections}
    </section>
  </>;
}

/**
 * @returns Component representing section of inspection report with detailed report information
 * 
 */
function Inspection({inspection}){
  const deficiencies = [];
  for(const deficiency of inspection.deficiencies){
    deficiencies.push(<li>{deficiency}</li>);
  }
  if(deficiencies.length === 0){
    deficiencies.push(<li>No deficiencies found</li>);
  }
  return <>
    <section className="inspection-card">
      <section className="inspection-card-header">
        <h3>{inspection.date.split('T')[0]}</h3>
        <p>{inspection.grade}</p>
      </section>
      <details>
        <summary>View Report Details</summary>
        <ul className="inspection-deficiency-list">
          {deficiencies}
        </ul>
      </details>
    </section>
  </>;
}