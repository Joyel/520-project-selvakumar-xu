import { Icon } from 'leaflet';
import { 
  MapContainer, 
  TileLayer, 
  Marker,
  Popup
} from 'react-leaflet';

import 'leaflet/dist/leaflet.css';
import './NewYorkMap.css';
import markerA from '../img/marker-a.png';
import markerB from '../img/marker-b.png';
import markerC from '../img/marker-c.png';
import { getLabels } from './Controls';


/**
 * @param {array} markers Array of objects to correspond to the map markers
 * @returns Entire map display with markers corresponding to the food establishments
 * 
 */
export default function NewYorkMapMap({markers, noData, updateReport}) {
  const attribution = 
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';
  const tileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
  return (
    <>
      <MapContainer
        center={[40.7028, -74.0000]}
        zoom={13}
        zoomControl={true}
        updateWhenZooming={false}
        updateWhenIdle={true}
        preferCanvas={true}
        minZoom={8}
        maxZoom={18}
        className="leaflet-container"
      >
        <TileLayer
          attribution={attribution}
          url={tileUrl}
        />    
        {
          markers.map(function(marker){
            const coordinates = marker.coordinates;
            const tradeName = marker.trade_name;
            const type = marker.establishment_type;
            const letterGrade = marker.inspection_grade;
            let theIconUrl = markerA;
            if(letterGrade === 'A'){
              theIconUrl = markerA;
            }else if(letterGrade === 'B'){
              theIconUrl = markerB;
            }else if(letterGrade === 'C'){
              theIconUrl = markerC;
            }

            let fullTypeName = '';
            for(const label of getLabels()){
              for(const singleType of type){
                if(label[3] === singleType){
                  fullTypeName += label[1] + '; ';
                }
              }
            }
            return (
              <Marker position={coordinates} icon={new Icon({
                iconUrl: theIconUrl,
                iconSize: [38, 38],
                iconAnchor: [22, 30]
              })} >
                <Popup>
                  <p><b>Name:</b> {tradeName}</p>
                  <p><b>Type:</b> {fullTypeName}</p>
                  <p><b>Inspection Grade:</b> {letterGrade}</p>
                  <p><b>Coordinates:</b> ({coordinates[0]}, {coordinates[1]})</p>
                  <button onClick={() => updateReport(tradeName)}>More Info</button>
                </Popup>
              </Marker>
            );
          })
        }
      </MapContainer>
      {noData}
    </>
  );
}
